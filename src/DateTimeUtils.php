<?php

namespace Rockschtar\WordPress\DateTimeUtils;

use DateTime;
use DateTimeZone;
use Jenssegers\Date\Date;
use WP_Post;


class DateTimeUtils {

    /**
     * Gets the WordPress Timezone
     * @deprecated Use wp_timezone() instead
     * @return DateTimeZone
     */
    public static function getWordPressDateTimeZone(): DateTimeZone {

        $tz = get_option('timezone_string');

        if (!$tz) {
            $offset = get_option('gmt_offset');
            $tz = timezone_name_from_abbr('', $offset * 3600, false);
        }

        return new DateTimeZone($tz);
    }

    /**
     * @param int $timestamp
     * @param string $separator
     * @param bool $time
     * @return string
     */
    public static function convertTimestampToWordPressDateString(int $timestamp, string $separator = ' ', bool $time = true): string {
        $datetime = self::convertTimestampToWordPressDateTime($timestamp);
        return self::convertDateTimeToWordPressDateString($datetime, $time, $separator);
    }

    /**
     * @param int $timestamp
     * @return DateTime
     */
    public static function convertTimestampToWordPressDateTime(int $timestamp): DateTime {
        $datetime = self::getWordPressDateTime();
        $datetime->setTimestamp($timestamp);
        return $datetime;
    }

    public static function getWordPressDateTime(): DateTime {
        $datetime = new Date();
        $datetime->setTimezone(wp_timezone());
        return $datetime;
    }

    /**
     * @param DateTime $datetime
     * @param bool $time
     * @param string $separator
     * @return string
     */
    public static function convertDateTimeToWordPressDateString(DateTime $datetime, bool $time = true, $separator = ' '): string {
        $datetime_format = get_option('date_format');
        if($time) {
            $datetime_format .= $separator . get_option('time_format');
        }

        return $datetime->format($datetime_format);
    }

    /**
     * @param $datetime_string
     * @param bool $time
     * @param string $separator
     * @return bool|DateTime
     */
    public static function createFromString(string $datetime_string, bool $time = true, $separator = ' ' ) {
        $datetime_format = self::getWordPressDateTimeFormat($time, $separator);
        return DateTime::createFromFormat($datetime_format, $datetime_string, wp_timezone());
    }

    public static function createFromFormat(string $datetime_format, string $datetime_string) {
        return DateTime::createFromFormat($datetime_format, $datetime_string, wp_timezone());
    }

    /**
     * @param bool $time
     * @param string $separator
     * @return null|string
     */
    final public static function getWordPressDateTimeFormat($time = true, $separator = ' ') : ?string {
        $datetime_format = get_option('date_format');

        if($time) {
            $datetime_format .= $separator . get_option('time_format');
        }

        return $datetime_format;
    }

    /**
     * @param int|WP_Post|null $post Optional. Post ID or post object. Defaults to global $post.
     * @return DateTime
     */
    final public static function getTheModifiedDateTime($post): ?DateTime {

        $the_post = get_post($post);

        if($the_post === null) {
            return null;
        }

        return self::convertWordPressDateStringToDateTime($the_post->post_modified_gmt);
    }

    /**
     * @param int|WP_Post|null $post Optional. Post ID or post object. Defaults to global $post.
     * @return DateTime
     */
    final public static function getTheDateTime($post): ?DateTime {

        $the_post = get_post($post);

        if($the_post === null) {
            return null;
        }

        return self::convertWordPressDateStringToDateTime($the_post->post_date_gmt);
    }

    private static function convertWordPressDateStringToDateTime(string $date_string): ?DateTime {

        $datetime_format = self::getWordPressDateTimeFormat();
        $date = DateTime::createFromFormat($datetime_format, $date_string, self::getWordPressDateTimeZone());

        if($date === false) {
            return null;
        }

        return $date;
    }

}
