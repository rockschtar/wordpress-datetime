<?php
/**
 * Plugin Name: WordPress DateTime
 * Plugin URI: https://gitlab.com/rockschtar/wordpress-datetime
 * Description: Helper functions for working with datetime in WordPress
 * Version: develop
 * Author: Stefan Helmer
 * Author URI: https://gitlab.com/rockschtar/
 * License: MIT License
 */

/**
 * Gets the WordPress Timezone
 * @deprecated Use wp_timezone() instead
 * @return DateTimeZone
 */
function wpdtu_get_datetime_zone(): \DateTimeZone {
    return \Rockschtar\WordPress\DateTimeUtils\DateTimeUtils::getWordPressDateTimeZone();
}

/**
 * @param bool $time
 * @return string
 */
function wpdtu_get_datetime_format(bool $time = true): string {
    return \Rockschtar\WordPress\DateTimeUtils\DateTimeUtils::getWordPressDateTimeFormat($time);
}

/**
 * @param int $timestamp
 * @return DateTime
 */
function wpdtu_timestamp_to_wp_datetime(int $timestamp): \DateTime {
    return \Rockschtar\WordPress\DateTimeUtils\DateTimeUtils::convertTimestampToWordPressDateTime($timestamp);
}

/**
 * @param int $timestamp
 * @param bool $time
 * @param string $separator
 * @return string
 */
function wpdtu_timestamp_to_wp_datetime_string(int $timestamp, bool $time = true, $separator = ' '): string {
    return \Rockschtar\WordPress\DateTimeUtils\DateTimeUtils::convertTimestampToWordPressDateString($timestamp, $separator, $time);
}

/**
 * @param DateTime $datetime
 * @param bool $time
 * @param string $separator
 * @return string
 */
function wpdtu_datetime_to_wp_datetime_string(\DateTime $datetime, bool $time = true, $separator = ' '): string {
    return \Rockschtar\WordPress\DateTimeUtils\DateTimeUtils::convertDateTimeToWordPressDateString($datetime, $time, $separator);
}

/**
 * @param string $datetime_format
 * @param string $datetime_string
 * @return bool|DateTime
 */
function wpdtu_create_from_format(string $datetime_format, string $datetime_string) {
    return \Rockschtar\WordPress\DateTimeUtils\DateTimeUtils::createFromFormat($datetime_format, $datetime_string);
}

/**
 * @param string $datetime_string
 * @return DateTime
 */
function wpdtu_string_to_wp_datetime(string $datetime_string): \DateTime {
    return \Rockschtar\WordPress\DateTimeUtils\DateTimeUtils::createFromString($datetime_string);
}

/**
 * @param WP_Post $post
 * @return DateTime
 */
function wpdtu_get_post_datetime(\WP_Post $post): \DateTime {
    return \Rockschtar\WordPress\DateTimeUtils\DateTimeUtils::getTheDateTime($post);
}

/**
 * @param WP_Post $post
 * @return DateTime
 */
function wpdtu_get_post_modified_datetime(\WP_Post $post): \DateTime {
    return \Rockschtar\WordPress\DateTimeUtils\DateTimeUtils::getTheModifiedDateTime($post);
}


