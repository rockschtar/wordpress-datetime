<?php
/**
 * @author: StefanHelmer
 */
namespace Rockschtar\WordPress\DateTimeUtils\Tests;
use Rockschtar\WordPress\DateTimeUtils\DateTimeUtils;

class MainTest extends \PHPUnit\Framework\TestCase {
    protected function setUp() {
        parent::setUp();
        \Brain\Monkey\setUp();
    }

    protected function tearDown() {
        \Brain\Monkey\tearDown();
        parent::tearDown();
    }

    public function testGetTimeZone(): void {

        \Brain\Monkey\Functions\stubs(['get_option' => function(string $option) {
            if($option === 'timezone_string') {
                return 'Europe/Paris';
            }

            if($option === 'gmt_offset') {
                return 1;
            }
        }]);

        $datetime_zone = DateTimeUtils::getWordPressDateTimeZone();

        $this->assertEquals('Europe/Paris', $datetime_zone->getName());
        
        \Brain\Monkey\Functions\stubs(['get_option' => function(string $option) {
            if($option === 'timezone_string') {
                return '';
            }

            if($option === 'gmt_offset') {
                return 5;
            }
        }]);

        $datetime_zone = DateTimeUtils::getWordPressDateTimeZone();

        $this->assertEquals('Asia/Karachi', $datetime_zone->getName());
        
    }


}